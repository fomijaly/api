package com.firstappapi.api.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.firstappapi.api.entity.Author;
import com.firstappapi.api.repository.AuthorRepo;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    private AuthorRepo authorRepo;

    @GetMapping("")
    public List<Author> getAll(){
        return this.authorRepo.findAll();
    }

    @PostMapping("")
    public Author postOne(@RequestBody Author author){
        return this.authorRepo.save(author);
    }
}
