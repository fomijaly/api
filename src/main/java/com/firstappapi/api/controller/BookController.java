package com.firstappapi.api.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.firstappapi.api.entity.Book;
import com.firstappapi.api.services.BookService;

@RestController
@RequestMapping("/books")
public class BookController {

    private BookService bookService;

    public BookController(BookService bs) {
        this.bookService = bs;
    }

    @GetMapping("")
    public List<Book> getBookList() {
        return this.bookService.getAllBooks();
    };

    @GetMapping("/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(this.bookService.getBookById(id));
        } catch (Exception e) {
            return new ResponseEntity<Book>(HttpStatus.OK);
        }
    }

    @GetMapping("/title/{title}")
    public List<Book> getBookByTitle(@PathVariable String title) {
        return this.bookService.getBookByTitle(title);
    }

    @PostMapping("")
    public Book postBook(@RequestBody Book book) {
        return this.bookService.createBook(book);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteBookById(@PathVariable Long id) {
        try {
            this.bookService.deleteBook(id);
            return ResponseEntity.ok(true);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Book> updateBook(@PathVariable Long id, @RequestBody Book book) {
        try {
            return ResponseEntity.ok(this.bookService.updateBook(id, book));
        } catch (Exception e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
    }

    
}
