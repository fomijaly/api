package com.firstappapi.api.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.firstappapi.api.entity.Category;
import com.firstappapi.api.repository.CategoryRepo;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryRepo categoryRepo;

    @GetMapping("")
    public List<Category> getAll(){
        return this.categoryRepo.findAll();
    }

    @PostMapping("")
    public Category postOne(@RequestBody Category category){
        return this.categoryRepo.save(category);
    }
}
