package com.firstappapi.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.firstappapi.api.entity.Author;

@Repository
public interface AuthorRepo extends JpaRepository<Author, Long> {
    public List<Author >findByNameContainingIgnoreCase(String name);
    
}