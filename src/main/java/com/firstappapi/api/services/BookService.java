package com.firstappapi.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.firstappapi.api.entity.Book;
import com.firstappapi.api.repository.BookRepo;

import jakarta.persistence.EntityNotFoundException;

@Service
public class BookService {
    private BookRepo bookRepo;

    public BookService(BookRepo br) {
        this.bookRepo = br;
    }

    public List<Book> getAllBooks(){
        return this.bookRepo.findAll();
    }

    public Book getBookById(Long id){
        Optional<Book> optionalBook= this.bookRepo.findById(id);

        if(optionalBook.isPresent()){
            return optionalBook.get();
        } else{
            throw new EntityNotFoundException("Aucun livre n'a été trouvé avc cet id : " + id);
        }
    }

    public List<Book> getBookByTitle(String title){
        return this.bookRepo.findByTitleContainingIgnoreCase(title);
    }

    public Book createBook(Book book){
        return this.bookRepo.save(book);
    }

    public void deleteBook(Long id){
        Optional<Book> optionalBook = this.bookRepo.findById(id);
        if(optionalBook.isPresent()){
            this.bookRepo.deleteById(id);
        } else {
            throw new EntityNotFoundException("Aucun film trouvé avec cet id : " + id);
        }
    }

    public Book updateBook(Long id, Book bookToUpdate){
        Optional<Book> optionalBook = this.bookRepo.findById(id);
        if(optionalBook.isPresent()) {
            Book bk = optionalBook.get();
            bk.setId(id);
            bk.setTitle(bookToUpdate.getTitle());
            return this.bookRepo.save(bk);    
        } else {
            throw new EntityNotFoundException("Le livre avec l'id : " + id + "à mettre à jour n'existe pas.");
        }
    }
}
